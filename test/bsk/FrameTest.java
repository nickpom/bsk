package bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void TestGetFirstAndSecondThrow() throws Exception{
		Frame frame=new Frame(2,4);
		assertEquals(2,frame.getFirstThrow());
		assertEquals(4,frame.getSecondThrow());

	}
	@Test
	public void testGetFrameScore() throws Exception{
		Frame frame=new Frame(2,6);
		assertEquals(8, frame.getScore());

	}
	@Test
	public void testGetBonus() throws Exception{
		Frame frame=new Frame(4,6);
		frame.setBonus(5);
		assertEquals(5, frame.getBonus());

	}
	@Test
	public void testIfIsSpare() throws Exception{
		Frame frame=new Frame(4,6);
		assertEquals(true, frame.isSpare());

	}
	@Test
	public void testGetFrameScoreWithSpareBonus() throws Exception{
		Frame frame=new Frame(2,6);
		frame.setBonus(4);
		assertEquals(12, frame.getScore());

	}
	@Test
	public void testIfIsStrike() throws Exception{
		Frame frame=new Frame(10,0);
		assertEquals(true, frame.isStrike());

	}
	@Test
	public void testGetFrameScoreWithStrikeBonus() throws Exception{
		Frame frame=new Frame(2,6);
		frame.setBonus(4);
		assertEquals(12, frame.getScore());

	}
	
}


/*
game.addFrame(new Frame(1,5));
game.addFrame(new Frame(3,6));
game.addFrame(new Frame(7,2));
game.addFrame(new Frame(3,6));
game.addFrame(new Frame(4,4));
game.addFrame(new Frame(5,3));
game.addFrame(new Frame(3,3));
game.addFrame(new Frame(4,5));
game.addFrame(new Frame(8,1));
game.addFrame(new Frame(2,6));*/