package bsk;

public class Game {
	private int nFrames;
	private Frame[] frame=new Frame[10];
	private int firstBonusThrow;
	private int secondBonusThrow;
	/**
	 * It initializes an empty bowling game.
	 * @throws BowlingException 
	 */
	public Game() throws BowlingException {
		nFrames=0;
		for(int i=0;i<9;i++) {
			frame[i]= new Frame(0,0);
		}
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.frame[nFrames]=frame;
		nFrames++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		// To be implemented
		return frame[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score=0;
		for(int i=0; i<10;i++) {
			if(i==9&&frame[i].isStrike())
				score+= getFirstBonusThrow() + getSecondBonusThrow() ;
			else if(i==9&&frame[i].isSpare())
				score+= getFirstBonusThrow();
			
			if(frame[i].isStrike()&&i<9) {
				if(frame[i+1].isStrike()&&i<8) {
					frame[i].setBonus(frame[i+1].getFirstThrow()+frame[i+2].getFirstThrow());
					score+=frame[i].getScore();
				}else if(frame[i].isStrike()&&i==8){
					frame[i].setBonus(frame[i+1].getFirstThrow()+getFirstBonusThrow());
					score+=frame[i].getScore();
				}else {
					frame[i].setBonus(frame[i+1].getFirstThrow()+frame[i+1].getSecondThrow());
					score+=frame[i].getScore();
				}
			}else if(frame[i].isSpare()&&i<9){
				frame[i].setBonus(frame[i+1].getFirstThrow());
				score+=frame[i].getScore();
				}else {
					score+=frame[i].getScore();
				}
			

		}
		return score;	
	}

}
